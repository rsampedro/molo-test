from simulation import Belt, Recipe

components = ['A', 'B']
out = 'X'
work_time = 3
n_steps = 100
belt_length = 3

if __name__ == '__main__':

    recipe = Recipe(components, out, work_time)
    belt = Belt(belt_length, components)
    belt.fill_workers(recipe)

    belt.run(n_steps)