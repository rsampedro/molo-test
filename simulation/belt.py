from random import randint
from .worker import Worker


class Belt():
    def __init__(self, size, components):
        # Improvement: Add workers at specific points of conveyor belt, not at all positions always, or different number
        # of parallel workers
        # Improvement: Add support for multiple recipes
        self.components = components
        self.belt = []
        self.size = size
        for x in range(size):
            slot = {'position': x,
                    'workers': [],
                    'content': None}
            self.belt.append(slot)
        self.endpoint = []

    def fill_workers(self, recipe):
        # Fill the belt with 2 workers all with the same recipe
        for idx in range(self.size):
            workers = []
            for i in range(2):
                worker = Worker(self.belt[idx], idx, recipe)
                workers.append(worker)
            self.belt[idx]['workers'] = workers

    def step(self):
        # Workers interact with queue
        for slot in self.belt:
            done = False
            for worker in slot['workers']:
                if not done:
                    done = worker.step()

        # Move the belt
        if self.belt[-1].get('content', None):
            self.endpoint.append(self.belt[-1]['content'])

        for idx in range(1, len(self.belt)):
            self.belt[-idx]['content'] = self.belt[-idx -1]['content']


        # Add new element
        idx = randint(0, len(self.components))
        if idx == 0:
            new_element = None
        else:
            new_element = self.components[idx-1]
        self.belt[0]['content'] = new_element

    def run(self, n_steps):
        workers = []
        for slot in self.belt:
            workers = workers + slot['workers']


        for t in range(n_steps):
            self.step()
            belt_state = [str(slot['content']) for slot in self.belt]
            out = "Step: {time} || Belt: ".format(time=t)+' >> '.join(belt_state)
            print (out)
            for worker in workers:
                print("Pos: {position} || Mode: {mode} || Inv: {inventory} || Worktime: {time}".format(position=worker.position,
                                                                                                       mode = worker.mode,
                                                                                                       inventory=worker.inventory,
                                                                                                       time=worker.work_time))
        dict_results = {}
        for el in self.endpoint:
            dict_results[el] = dict_results.get(el, 0)+1

        print("Done! Results: {endpoint}".format(endpoint = self.endpoint))
        print (dict_results)


