class Worker():
    def __init__(self, slot, position, recipe):
        # Modes =
        #   search: Searching for the items to start working
        #   out: Trying to output result
        #   work: working
        self.mode = 'search'
        self.slot = slot
        self.work_time = 0
        self.position = position    #Only for output and debugging purposes
        self.recipe = recipe
        self.inventory = []
        self.inventory_size = 2

    def step(self):
        return_value = False
        if self.mode == 'search':
            return_value = self._search()
        elif self.mode == 'work':
            return_value = self._work()
        return return_value

    def _out(self):
        return_value = False
        if not self.slot.get('content', None):
            if self.recipe.output in self.inventory:
                self.inventory.remove(self.recipe.output)
                self.slot['content'] = self.recipe.output
                self.mode = 'search'
                return_value = True
        return return_value

    def _search(self):
        return_value = False
        current_item = self.slot.get('content', None)
        if len(self.inventory) < self.inventory_size:
            if current_item in self.recipe.ingredients and current_item not in self.inventory:
                self.inventory.append(current_item)
                self.slot['content'] = None
                return_value = True

        # If i have a product in the inventory put it in the conveyor belt
        if not self.slot.get('content', None):
            if self.recipe.output in self.inventory:
                self.inventory.remove(self.recipe.output)
                self.slot['content'] = self.recipe.output
                return_value = True

        if set(self.inventory) == set(self.recipe.ingredients):
            self.mode = 'work'

        return return_value

    def _work(self):
        self.work_time += 1
        if self.work_time >= self.recipe.time:
            self.inventory = [self.recipe.output]
            self.work_time = 0
            self.mode = 'search'



