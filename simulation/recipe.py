class Recipe():
    # Right now I only contemplate that recipes need 1 item of each category
    def __init__(self, ingredients, output, time):
        self.ingredients = list(set(ingredients))
        self.output = output
        self.time = time