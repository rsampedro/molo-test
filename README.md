# Technical Challenge

## Problem description

Check ``exercise.md`` for details

## Installation

1. Clone this repo

2. Run with python 3.6 (Preferrably with a virtual environment)

``virtualenv``

## Usage

Run main.py, after editing the parameters inside

## Assumptions/Comments

1. Limitations on inventory of the workers can be easily limited
2. I added some support for different product recipes, though I assumed you only ever needed at most 1 of each of the ingredients.
3. Order of operations in the main loop can be modified according to need, since they've been encapsulated.
